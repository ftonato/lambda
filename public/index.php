<?php
/**
 * PHP λ::lambda(); // Lightweight PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

// Bootstrap function for autoload classes.
require_once '../bootstrap.php';



use CORE\λ as λ;


λ::route(
    '/',
    function () {
        λ::view('layout', ['content' => λ::view('main', [], true)]);
    }
);


λ::public('assets', ['css', 'woff', 'woff2', 'png', 'json', 'js', 'svg']);
