# λ::Lambda

Lambda is a simple, light, modern and extremely adaptable framework written in PHP 7 where you can use the pattern that best fits the system you are developing.

If you already used to these Laravel, Lumen, CodeIgniter among others, I'm sure we will be happy together.



# Installation

Install using composer run in your terminal the command `composer create-project php-developer/lambda your-project-name dev-master` or simply clone the project from `https://gitlab.com/php-developer/lambda`.



# License and terms of use

The author is not liable for misuse and/or damage caused by free use and/or distribution of this tool.

## Creative Commons 4.0

(cc) 2016, λ::Lambda - John Murowaniecki. Some rights reserved.
