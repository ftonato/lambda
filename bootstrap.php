<?php
/**
 * PHP λ::lambda(); // Lightweight PHP7 Framework - Bootstrap file.
 *
 * PSR recomends to use SPL functions as a good practice.
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

spl_autoload_register(
    function ($classpath) {
        $module = str_replace('\\', DIRECTORY_SEPARATOR, "{$classpath}.php");

        try {
            include_once $module;
        } catch (Exception $e) {
            echo "An error ocurred: {$e->getMessage()}";
        }
    }
);

CORE\λ::prepare();
