<!DOCTYPE html>
<html>
  <head>
    <base href="<?php echo self::URI()->root_url ?>" target="_self">
    <meta charset="utf-8">

    <title>λ::lambda {} // Lightweight PHP7 framework</title>

    <meta name="author" content="John Murowaniecki">
    <meta name="description" content="Lightweight PHP 7 framework">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/img/logo.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" type="text/css" href="assets/style.css">

    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/logo.png">
    <link rel="manifest" href="assets/manifest.json">
    <link rel="mask-icon" href="assets/img/logo.png" color="#990000">
    <link rel="shortcut icon" href="assets/img/logo.png" type="image/png">
  </head>

  <body>
  <div class="nav">
    <h1 class="λ">λ</h1>
    <h2>::lambda</h2>
    <h3>// Lightweight PHP7 framework</h3><!-- ƒ (∀) { ∴ } -->
  </div>

<?php echo $content ?>

  </body>
</html>
