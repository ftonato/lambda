<?php
/**
 * PHP λ::lambda(); // Lightweight PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

namespace CORE;

/**
 * CORE class λ
 *
 * @category CORE_Class
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/
class λ
{

    public static $maintainer = null;

    public static $contact = null;

    public static $show404 = false;

    /**
     * Set static variables with developers config.
     *
     * @param Array $config Collection of values to set.
     * @return void
     **/
    public static function init(Array $config = [])
    {
        foreach ($config as $key => $value) {
            self::${$key} = $value;
        }
    }

    /**
     * Return the requested URI.
     *
     * @return String
     **/
    public static function requestURI(): String
    {
        return (php_sapi_name() == 'cli'
            ? $_SERVER['argv'][1]
            : $_SERVER['REQUEST_URI']);
    }


    /**
     * Set proper routing based on regex pattern assign to a callable function.
     *
     * @param  String   $regex    Request pattern.
     * @param  Callable $function Callable function.
     * @param  Boolean  $continue Continue the flow after callback execution.
     * @return void
     **/
    public static function route(String $regex, $function, bool $continue = false)
    {
        $regex = str_replace('/', '\/', $regex);

        if (preg_match("/{$regex}\$/i", '/' . self::URI()->route, $match) === 1) {
            λ::$show404->rigged = false;
            array_shift($match);

            $return = call_user_func_array($function, $match ?? false);

            if ($continue && php_sapi_name() == 'cli' || (bool) $return) {
                return $return;
            }
            exit;
        }
    }


    /**
     * Rig the automatic 404 error handler.
     *
     * @return void
     **/
    public static function prepare()
    {
        if (λ::$show404 == false) {
            λ::$show404 = new class
            {

                public $rigged = true;

                /**
                 * Automatic callback function to handle 404 errors.
                 *
                 * @return void
                 **/
                public function __destruct()
                {
                    if (!$this->rigged) {
                        return;
                    }

                    header("{$_SERVER['SERVER_PROTOCOL']} 404 Not Found", true, 404);
                    λ::view('layout', ['content' => λ::view('errors/404', [], true)]);
                }
            };
        }

    }


    /**
     * Set public folder.
     *
     * @param String $folder     Folder name.
     * @param Array  $extentions Allowed extentions.
     * @return void
     **/
    public static function public($folder, Array $extentions = [])
    {
        $req       = self::requestURI();
        $check     = strpos($req, '?');
        $file      = substr($req, 1, $check ? $check - 1 : strlen($req));
        $extention = substr($file, strrpos($file, '.') + 1, strlen($file));

        if (preg_match("/\/{$folder}\//i", $req, $matches) === 1 &&
            is_dir($folder) &&
            in_array($extention, $extentions) &&
            file_exists($file)) {
            switch ($extention) {
                case 'css':
                    header('Content-Type: text/css');
                    break;

                case 'js':
                    header('Content-Type: application/javascript');
                    break;

                case 'woff2':
                case 'woff':
                case 'ttf':
                    header("Content-Type: application/font-{$extention}");
                    break;

                case 'svg':
                    header('Content-Type: image/svg+xml');
                    break;

                case 'png':
                    header('Content-Type: image/png');
                    break;

                default:
                    break;
            }

            λ::$show404->rigged = false;
            $return = file_get_contents($file);

            if (php_sapi_name() == 'cli') {
                return $return;
            }
            echo $return;
            exit;
        }
    }


    /**
     * Module loader.
     *
     * @param String $module Module name.
     * @return Object Module
     **/
    public static function module(String $module): Object
    {
        $module = strtolower($module);
        $is_dir = is_dir("modules/{$module}");

        $modulePath = ($is_dir
            ? "modules/{$module}/controller.php"
            : "modules/{$module}.php");

        if (file_exists($modulePath)) {
            include_once $modulePath;

            return new $module([
                'path' => dirname($modulePath) . '/',
                ]);
        }
    }


    /**
     * Returns a class to handle URI data.
     *
     * @return void
     **/
    public static function URI()
    {
        return new URI();
    }

    /**
     * Handle views, layout and template files.
     *
     * @param String  $file   View/layout filename/path.
     * @param Array   $data   Data collection.
     * @param Boolean $return Used if the view must be loaded to a variable.
     * @return String/Void
     **/
    public static function view(String $file, Array $data = [], $return = false)
    {
        foreach ($data as $key => $value) {
            $$key = $value;
        }

        ob_start();

        $source = "../views/{$file}.php";

        if (!file_exists($source)) {
            $source = '../modules/' . λ::URI(1) . "/{$source}";
        }
        include $source;

        $output = ob_get_contents();
        ob_end_clean();

        $elapsed = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];
        $output  = str_replace('{elapsed_time}', $elapsed, $output);

        if ($return) {
            return $output;
        }

        echo $output;
    }
}
