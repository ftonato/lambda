<?php
/**
 * PHP λ::lambda(); // Lightweight PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

namespace CORE;

/**
 * CORE class URI
 *
 * @category CORE_Class
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/
class URI
{

    public $protocol;

    public $hostname;

    public $port;

    public $host;

    public $root_url;

    public static $base_url;

    /**
     * Cut text based on a mark.
     *
     * @param String $text Text to be cut.
     * @param String $mark Mark to cut to.
     * @return String
     **/
    public function cut(String $text = '', String $mark = ''): String
    {
        $size = strpos($text, $mark);
        return $size < 1 ? $text : substr($text, 0, $size);
    }

    /**
     * Class constructor.
     **/
    public function __construct()
    {
        $URI                = λ::requestURI();
        $URI                = $URI[0] == '/' ? substr($URI, 1) : $URI;
        $check              = strpos($URI, '?');
        $this->host         = $_SERVER['HTTP_HOST'];
        $this->port         = $_SERVER['SERVER_PORT'];
        $this->proto        = $_SERVER['SERVER_PROTOCOL'];
        $this->proto        = strtolower($this->cut($this->proto ?? '', '/'));
        $this->route        = $this->cut($URI, '?');
        $this->hostname     = $this->cut($this->host ?? '',  ':');
        $this->query_string = $check < 1 ? null : substr($URI, $check + 1);
        self::$base_url     = $this->root();
    }

    /**
     * Magic method to convert object data to a String.
     *
     * @return String
     **/
    public function __toString()
    {
        return $this->root();
    }

    /**
     * Returns root segment data.
     *
     * @return String
     **/
    public function root()
    {
        if ($this->root_url == null) {
            $this->root_url = "{$this->proto}://{$this->host}/";
        }
        return $this->root_url;
    }

    /**
     * Return segments.
     *
     * @return array
     **/
    public function segments(): Array
    {
        if ($this->segments == false) {
            $this->segments = explode(
                '/',
                substr($this->cut($this->route, '?'), 1)
            );
        }
        return $this->segments ?? [];
    }

    /**
     * Return just one segment.
     *
     * @param Integer $segment Segment number.
     * @return String
     **/
    public function segment(int $segment = 0)
    {
        return $this->segments[$segment] ?? null;
    }
}
