<div class="dashboard">
  <h1><em>Error:</em> 404</h1>
  <h2>// Page or resource not found.</h2>
  <h3>Check the address and try again.</h3>
  <p>You're trying to access `<a href="<?php echo self::URI() . self::URI()->route?>"><?php echo self::URI()->route ?></a>` but didn't have any rule or expression for this route. Maybe the content isn't avaliable anymore or did occour some error.</p>
  <p>Try refreshing the page, or going back and attempting the action again.</p>
  <p>Please contact the administrator if this problem persists.</p>
  <p>Rendered in {elapsed_time} seconds.</p>
</div>
