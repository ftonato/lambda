<?php
/**
 * PHP λ::lambda(); // Lightweight PHP7 Framework
 *
 * Testing case module.
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

namespace Testing;

use PHPUnit\Framework\TestCase;
use CORE\λ as λ;

/**
 * ModulesTest
 *
 * @category Testing_Module
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/
class ModulesTest extends TestCase
{
    /**
     * Test if bootstrap is loading the framework correctly.
     *
     * @return void
     **/
    public function testBootstrap()
    {
        $this->assertEquals(
            42,
            λ::route(
                '',
                function () {
                    return 42;
                }
            )
        );
    }
}
